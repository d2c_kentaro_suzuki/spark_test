package examples

import org.apache.spark._
import org.apache.spark.SparkContext._

object WordCount extends Logging {
  def main(args: Array[String]) {
    val conf = new SparkConf().setAppName("spark_test")
    val sc = new SparkContext(conf)
    val files = sc.textFile("hdfs://localhost:9000/data/Word_Count_input.txt")
    val words = files.flatMap(_.split(" "))
    val wordCounts = words.map(s => (s, 1)).reduceByKey(_ + _)
    wordCounts.saveAsTextFile("hdfs://localhost:9000/result")
  }
}
